// Make sure to check if the right port and board are selected.
// https://www.epochconverter.com/ for time sync. Input into serial monitor after startup: T0123456789
#include <MechaQMC5883.h> // Library for the magnetometer used in our pilot. Others are possible, but it would require a slight change in code. Library can be downloaded here: https://www.arduino.cc/reference/en/libraries/qmc5883lcompass/
#include <SPI.h> // Normally part of the Arduino core libraries, no download is needed
#include <SD.h> // Can be found in the library manager when searching "SD"
#include <TimeLib.h> // Can be found in the library manager when searching "timelib"
const int chipSelect = 4;
unsigned long TimeAfterStart;
int counts = 0;
float V = 0;
boolean Headers = true;
MechaQMC5883 qmc;
#define cellPin A5 // The pin at which we read the voltage data, can be changed.
#define TIME_HEADER  "T"
#define TIME_REQUEST  7

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // Wait for serial port to connect. Needed for native USB port only
  }

  Serial.print("Initializing SD card...");

  // See if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    while (1);
  }
  Serial.println("card initialized.");
  
  Wire.begin();             // Setup I2C bus
  Wire.setClock(400000);    // I2C fast mode, 400kHz

  qmc.init();
}

void loop() {
  int x, y, z;
  File MagMeterData = SD.open("MData.txt", FILE_WRITE); // File name can be changed
  if (Serial.available()) {
    processSyncMessage();
  }
  if (timeStatus()!= timeNotSet) {
    digitalClockDisplay();  
  }
  if (timeStatus() == timeSet) {
    digitalWrite(13, HIGH); // LED on if synced
  } else {
    digitalWrite(13, LOW);  // LED off if needs refresh
  }
  delay(200); // Delay can be modified to increase or decrease the frequency of data output
  
  if (MagMeterData) {
  // Get X Y and Z data at once
qmc.read(&x,&y,&z);
  // Show the data in the serial monitor
  Serial.print("x: ");
  Serial.print(x);
  Serial.print(" y: ");
  Serial.print(y);
  Serial.print(" z: ");
  Serial.println(z);
  
  TimeAfterStart = millis();
  Serial.print(TimeAfterStart); Serial.println(" ms");
  counts = analogRead(cellPin);
  V = (3.28/1023.)*counts;
  Serial.print(V); Serial.println(" V") ;
  delay(150);
  
  // Write the data on the SD card
  if (year()>2000){
    if (Headers == true){
  MagMeterData.println("x(uT),y(uT),z(uT),Volt(V),Date,TimeAfterStart(ms)");
  Headers = false;
  } 
  // Sensor data and voltage
  MagMeterData.print(x);
  MagMeterData.print(",");
  MagMeterData.print(y);
  MagMeterData.print(",");
  MagMeterData.print(z);
  MagMeterData.print(",");
  MagMeterData.print(V);
  MagMeterData.print(",");
  // Time data
  if (hour()<10){
    MagMeterData.print("0");
  }
  MagMeterData.print(hour());
  MagMeterData.print(":");
  if (minute()<10){
    MagMeterData.print("0");
  }
  MagMeterData.print(minute());
  MagMeterData.print(":");
  if (second()<10){
    MagMeterData.print("0");
  }
  MagMeterData.print(second());
  MagMeterData.print(" ");
  MagMeterData.print(day());
  MagMeterData.print(" ");
  MagMeterData.print(month());
  MagMeterData.print(" ");
  MagMeterData.print(year());
  MagMeterData.print(",");
  MagMeterData.println(TimeAfterStart);
  MagMeterData.close();
  }
  } else {
  Serial.println("Unable to open file");
  }
}
void digitalClockDisplay(){
  // Print the time in the serial monitor
  Serial.print(hour());
  Serial.print(":");
  Serial.print(minute());
  Serial.print(":");
  Serial.print(second());
  Serial.print(" ");
  Serial.print(day());
  Serial.print(" ");
  Serial.print(month());
  Serial.print(" ");
  Serial.print(year()); 
  Serial.println(); 
}
void processSyncMessage() {
  unsigned long pctime;
  const unsigned long DEFAULT_TIME = 1357041600; 

  if(Serial.find(TIME_HEADER)) {
     pctime = Serial.parseInt();
     if( pctime >= DEFAULT_TIME) { 
       setTime(pctime); // Sync Arduino clock to the time received on the serial port
     }
  }
}
time_t requestSync()
{
  Serial.write(TIME_REQUEST);  
  return 0; 
}
